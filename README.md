# ConsoleInput-in-Java



import java.io.*;

public class ConsoleInput {
 
 public static void main(String[] args){
  
  try {
   // Create object ins and connect it to the console
   InputStreamReader inStream = new InputStreamReader(System.in);
   BufferedReader ins = new BufferedReader(inStream);
   
   // Read name of the output file.
   System.out.print("Enter output file name: ");
   String outFileName = ins.readLine();
   
   // Create object outs - the output file.
   FileWriter outStream = new FileWriter(outFileName);
   PrintWriter outs = new PrintWriter(outStream);
   
   //Get the number of data lines.
   System.out.println("How many data lines?");
   int numLines = Integer.parseInt(ins.readLine());
   
   // Read the data lines from the console
   // and write them to the output file
   System.out.println("Type " + numLines + " lines:");
   for (int lineCount = 0; lineCount < numLines; lineCount++) {
   String dataLine = ins.readLine();
 }
 
 // Write message to console and close files
  System.out.println(numLines + " data lines written to file " + outFileName);
  ins.close();
  outs.close();
 }
  catch (IOException ex) {
  System.out.println("i/o error:" + ex.getMessage());
  ex.printStackTrace();
 }
 catch (NumberFormatException ex){
   System.out.println(ex.getMessage());
  }
 }
}      
   
   
   
